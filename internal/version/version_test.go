package version

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Version(t *testing.T) {
	version = "0"
	goVersion = "go1"
	buildDate = "2020"

	t.Run("verify Obtain() function - missing Git", func(t *testing.T) {
		assert.Equal(t, "Version: 0\nGo Version: go1\nBuilt: 2020\n", Obtain())
	})

	gitCommit = "abc"
	gitBranch = "test"

	t.Run("verify Obtain() function - Git present", func(t *testing.T) {
		assert.Equal(
			t,
			"Version: 0\nGit Commit: abc\nGit Branch: test\nGo Version: go1\nBuilt: 2020\n",
			Obtain(),
		)
	})

	t.Run("verify Version() function", func(t *testing.T) {
		assert.Equal(t, "0", Version())
	})
}
