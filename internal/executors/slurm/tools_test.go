package slurm

import (
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func Test_sacctState(t *testing.T) {
	tests := []struct {
		name string
		out  string
		want string
	}{
		{
			name: "Multi-line sacct return",
			out: `TIMEOUT
CANCELLED`,
			want: "TIMEOUT",
		}, {
			name: "Single-line sacct return",
			out:  "COMPLETED",
			want: "COMPLETED",
		}, {
			name: "Empty sacct return",
			out:  "",
			want: "",
		},
	}
	for _, tt := range tests {
		got := sacctState(tt.out)
		assert.Equal(t, tt.want, got, tt.name)
	}
}

// sacct (https://slurm.schedmd.com/sacct.html) interaction testing
func Test_Mock_Sacct(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := buildMockSlurm(ctrl)

	tests := map[string]slurmTests{
		"sacct identifies completed job": {
			stdin:   "sacct 100",
			timeout: 0 * time.Second,
			run:     m,
			wantErr: false,
		},
		"sacct identifies cancelled job": {
			stdin:   "sacct 200",
			timeout: 0 * time.Second,
			run:     m,
			wantErr: true,
		},
		"sacct pending -> completed job": {
			stdin:   "sacct 300",
			timeout: 0 * time.Second,
			run:     m,
			wantErr: false,
		},
		"sacct command failure": {
			stdin:   "sacct 600",
			timeout: 0 * time.Second,
			run:     m,
			wantErr: true,
		},
		"sacct stuck on completing job": {
			stdin:   "sacct 700",
			timeout: 5 * time.Second,
			run:     m,
			wantErr: true,
			assertErrorMsg: func(t *testing.T, err error) {
				assert.Equal(
					t,
					"slurm job failed, stage stuck in COMPLETING",
					err.Error(),
				)
			},
		},
		"sacct return empty state": {
			stdin:   "sacct 999",
			timeout: 0 * time.Second,
			run:     m,
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := obtainSacct(tt.stdin, tt.timeout, tt.timeout, tt.run)

			assert.Equal(t, tt.wantErr, err != nil)

			if tt.assertErrorMsg != nil {
				tt.assertErrorMsg(t, err)
			}
		})
	}
}

func Test_sacctStdin(t *testing.T) {
	t.Run("Sacct stdin creation", func(t *testing.T) {
		s := sacctStdin("sacct", "123")
		assert.Equal(
			t,
			"sacct -n -j 123 -L --format=state -S "+
				time.Now().AddDate(0, 0, -1).Format("01/02/06"),
			s,
		)
	})
}

func Test_outputFile(t *testing.T) {
	t.Run("Update file", func(t *testing.T) {
		f := outputFile("123", "/var/tmp/slurm-%j.out")
		assert.Equal(t, "/var/tmp/slurm-123.out", f)
	})
}

func Test_sbatchJobID(t *testing.T) {
	tests := []struct {
		name    string
		out     string
		want    string
		wantErr bool
	}{
		{
			name:    "Standard output",
			out:     "Submitted batch job 123\n",
			want:    "123",
			wantErr: false,
		}, {
			name:    "Cluster specific return",
			out:     "Submitted batch job 456789 on cluster example",
			want:    "456789",
			wantErr: false,
		}, {
			name:    "Debug output",
			out:     "sbatch: slurm_spank_init nodes=0\nsbatch: argc[0] is host=localhost\nsbatch: argc[1] is test=true\nSubmitted batch job 5\nsbatch: slurm_spank_exit",
			want:    "5",
			wantErr: false,
		}, {
			name:    "No jobid",
			out:     "Submitted batch job",
			want:    "",
			wantErr: true,
		}, {
			name:    "Multiple jobID found",
			out:     "TEST\nSubmitted batch job 1\nSubmitted batch job 2",
			want:    "1",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := sbatchJobID(tt.out)
			assert.Equal(t, tt.wantErr, err != nil)
			if err == nil {
				assert.Equal(t, tt.want, got, "sbatchJobID() return value")
			}
		})
	}
}
