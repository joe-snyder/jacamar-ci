package lsf

import (
	"errors"
	"fmt"
	"os/exec"

	"gitlab.com/ecp-ci/jacamar-ci/internal/batch"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
)

type executor struct {
	absExec *executors.AbstractExecutor
	mng     batch.Manager
}

func (e *executor) Prepare() error {
	_, err := exec.LookPath(
		batch.PrefixSchedulerPath("bsub", e.absExec.Cfg.Batch().SchedulerBin),
	)
	if err != nil {
		return errors.New("unable to locate LSF (bsub) in the CI environment")
	}

	return nil
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}
	bsubStdin := fmt.Sprintf("%s -I %s", e.mng.BatchCmd(), e.mng.UserArgs())

	err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, bsubStdin)

	// Wait for configured NFS timeout window (regardless of error).
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	return err
}

func (e *executor) Cleanup() error {
	return nil
}

// NewExecutor generates a valid LSF executor that fulfills the executors.Executor interface.
func NewExecutor(ae *executors.AbstractExecutor) (e *executor) {
	e = &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd:    "bsub",
			IllegalArgs: []string{"-e", "-o", "-eo", "-I"},
		}
		e.mng = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
	}

	return
}
