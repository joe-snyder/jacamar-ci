package augmenter

import (
	"bytes"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type rTests struct {
	stage    string
	filepath string
	dir      string
	token    string
	r        Rules

	mockSomething func(*testing.T, string)
	assertError   func(*testing.T, error)
	assertString  func(*testing.T, string)
	assertSlice   func(*testing.T, []string)
}

func TestRules_JobScript(t *testing.T) {
	tests := map[string]rTests{
		"nonexistent filepath provided": {
			filepath: "/file/does/not/exist",
			stage:    "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"existing prepare_script, ensure no changes are made currently": {
			filepath: "../../test/testdata/job-scripts/ci/prepare_script",
			stage:    "prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, `#!/usr/bin/env bash

set -eo pipefail
set +o noclobber
: | eval $'echo "Running on $(hostname)..."\n'
exit 0
`, s)
			},
		},
		"identify and remove target redacted environment variables only": {
			filepath: "../../test/testdata/job-scripts/ci/redactedenvvar_test",
			stage:    "after_script",
			r: Rules{
				UnrestrictedCmdline:  true,
				AllowUserCredentials: true,
				RedactedEnvVars:      []string{"FOO", "BAR"},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash")
				cmd.Stdin = bytes.NewBufferString(s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "functional script expected")
				assert.Equal(
					t,
					"pass VALUE_REMOVED VALUE_REMOVED\n",
					string(out),
					"expected variables not removed",
				)
			},
		},
		"verify GIT_ASKPASS variable set for job script": {
			filepath: "../../test/testdata/job-scripts/ci/askpass_test",
			stage:    "build_script",
			dir:      "/builds/0/test",
			r: Rules{
				UnrestrictedCmdline:  false,
				AllowUserCredentials: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash")
				cmd.Stdin = bytes.NewBufferString(s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "functional script expected")
				assert.Equal(
					t,
					"/builds/0/test/.git/credentials/pass\n",
					string(out),
					"expected variables not removed",
				)
			},
		},
		"modify get_sources scripts to remove token from URLs": {
			filepath: "../../test/testdata/job-scripts/13.0.0/get_sources",
			stage:    "get_sources",
			dir:      "/builds/0/test",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				re := regexp.MustCompile(
					`"origin"\s"https://gitlab-ci-token@gitlab\.example\.com/user/scratch-space\.git"`,
				)
				match := re.FindAllStringIndex(s, -1)
				assert.Equal(
					t,
					2,
					len(match),
					"expected that two add remote commands have been updated",
				)
			},
		},
		"ensure that --token has been removed from artifact command": {
			filepath: "../../test/testdata/job-scripts/13.0.0/get_sources",
			stage:    "download_artifacts",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.False(
					t,
					strings.Contains(s, "--token"),
					"no --token argument should be found",
				)
				assert.True(t, strings.Contains(s, "export CI_JOB_TOKEN="))
			},
		},
		"verify rules established for broker (replace token & url)": {
			filepath: "../../test/testdata/job-scripts/ci/broker_rules",
			stage:    "after_script",
			r: Rules{
				TrustedHost:     "gitlab.example.gov",
				TargetHost:      "broker.example.gov",
				TrustedJobToken: "ciJobToken",
				TargetJobToken:  "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.NotContains(t, s, "ciJobToken")
				assert.NotContains(t, s, "gitlab.example.gov")
			},
		},
		"verify GIT_ASKPASS variable set for job step_script": {
			filepath: "../../test/testdata/job-scripts/ci/askpass_test",
			stage:    "step_script",
			dir:      "/builds/0/test",
			r: Rules{
				UnrestrictedCmdline:  false,
				AllowUserCredentials: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash")
				cmd.Stdin = bytes.NewBufferString(s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "functional script expected")
				assert.Equal(t, "/builds/0/test/.git/credentials/pass\n", string(out))
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockSomething != nil {
				tt.mockSomething(t, tt.filepath)
			}

			got, err := tt.r.JobScript(tt.filepath, tt.stage, tt.dir)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func Test_openScript(t *testing.T) {
	tests := map[string]rTests{
		"nonexistent filepath provided": {
			filepath: "/file/does/not/exist",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"simple, existing prepare_script": {
			filepath: "../../test/testdata/job-scripts/13.0.0/prepare_script",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertSlice: func(t *testing.T, s []string) {
				assert.Equal(t, "#!/usr/bin/env bash", s[0])
				assert.Equal(t, "", s[1])
				assert.Equal(t, "exit 0", s[5])
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := openScript(tt.filepath)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}

func TestCreateGitAskpass(t *testing.T) {
	tempDir, err := ioutil.TempDir(t.TempDir(), "gitaskpass")
	assert.NoError(t, err)
	_ = os.Mkdir(tempDir+"/.git", 0700)

	tests := map[string]rTests{
		"a .git directory does not exists": {
			dir: tempDir + "/",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"directory does not exist": {
			dir: "/does/not/exist",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "mkdir /does/not/exist/.git: no such file or directory")
			},
		},
		"generate valid credentials script": {
			dir:   tempDir,
			token: "T0k3n",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash", s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "error attempting to execute generated script")
				assert.Equal(t, "T0k3n\n", string(out))
			},
		},
		"overwrite an existing valid credentials script": {
			dir:   tempDir,
			token: "NEW_T0k3n",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				cmd := exec.Command("bash", s)
				out, err := cmd.CombinedOutput()
				assert.NoError(t, err, "error attempting to execute generated script")
				assert.Equal(t, "NEW_T0k3n\n", string(out))
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockSomething != nil {
				tt.mockSomething(t, tt.dir)
			}

			err := CreateGitAskpass(tt.dir, tt.token)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}

			if tt.assertString != nil {
				// Overload in this case to support file verification.
				tt.assertString(t, tt.dir+CredentialsLoc)
			}
		})
	}
}

func Test_gitRemoveToken(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "receive generic https url",
			s:    "https://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git",
			want: "https://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
		}, {
			name: "receive generic http url",
			s:    "http://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git",
			want: "http://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
		}, {
			name: "job token has already been removed",
			s:    "http://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
			want: "http://gitlab-ci-token@gitlab.example.com/user/scratch-space.git",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := gitRemoveToken(tt.s); got != tt.want {
				t.Errorf("gitRemoveToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_artifactsExportToken(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "artifact-uploader command, token provided",
			s:    `$\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/" "--token" "ciJobToken" "--id" "12"`,
			want: `export CI_JOB_TOKEN=$\'ciJobToken\' && $\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/"  "--id" "12"`,
		}, {
			name: "artifact-downloader command, token provided",
			s:    `$\'gitlab-runner\' "artifacts-downloader" "--token" "ciJobToken"`,
			want: `export CI_JOB_TOKEN=$\'ciJobToken\' && $\'gitlab-runner\' "artifacts-downloader" `,
		}, {
			name: "artifact-uploader command, no token provided",
			s:    `$\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/" "--id" "12"`,
			want: `$\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/" "--id" "12"`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := artifactsExportToken(tt.s); got != tt.want {
				t.Errorf("artifactsExportToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGitAskpassDir(t *testing.T) {
	t.Run("ensure default credentials folder returned", func(t *testing.T) {
		got := GitAskpassDir("/builds/dir")
		assert.Equal(t, "/builds/dir/.git/credentials", got, "no suffix provided")

		got = GitAskpassDir("/builds/dir/")
		assert.Equal(t, "/builds/dir/.git/credentials", got, "suffix handled")
	})
}

func Test_urlHost(t *testing.T) {
	tests := map[string]struct {
		s    string
		want string
	}{
		"invalid url provided": {
			// Ensuring the validity of a url string is not in the scope of the package.
			s:    "notaurl",
			want: "notaurl",
		},
		"host only defined": {
			s:    "gitlab.example.com",
			want: "gitlab.example.com",
		},
		"empty string": {
			s:    "",
			want: "",
		},
		"traditional url": {
			s:    "https://gitlab.example.com",
			want: "gitlab.example.com",
		},
		"traditional url with port": {
			s:    "https://gitlab.example.com:3000",
			want: "gitlab.example.com:3000",
		},
		"traditional url with slash": {
			s:    "https://gitlab.example.com/",
			want: "gitlab.example.com",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if got := urlHost(tt.s); got != tt.want {
				t.Errorf("urlHost() = %v, want %v", got, tt.want)
			}
		})
	}
}
