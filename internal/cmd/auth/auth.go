// Package auth realizes the core of the jacamar-auth application by ensuring the realization
// of the process of authorizing the already authenticated  user provided by the GitLab server.
package auth

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/brokercomms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command/bash"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
)

var (
	// Function to facilitate associated process exit.
	sysExit, buildExit func()
)

const (
	jacamarApp = "jacamar"
)

// Start initializes the jacamar-auth application process, requires valid command arguments and
// custom executor provided environmental variables.
func Start(c arguments.ConcreteArgs) {
	msg := logging.NewMessenger()

	// Close any Commander related GoRoutines.
	done := make(chan struct{})
	defer close(done)

	if c.Cleanup != nil {
		// Only fail cleanup during explicit cleanup errors, not
		// due to a repeat of an error from config.
		sysExit = func() { os.Exit(0) }
	}

	ae, err := establishAbstract(c, msg, done)
	if err != nil {
		// All errors encountered here will be considered system errors, even if they are
		// caused by a user influenced setting.
		errorhandling.MessageError(c, msg, err)
		sysExit()
		return
	}

	switch {
	case c.Config != nil:
		preparations.ConfigExec(ae, c, sysExit)
	case c.Prepare != nil:
		prepareExec(ae)
	case c.Run != nil:
		runExec(ae, c)
	case c.Cleanup != nil:
		cleanupExec(ae, c)
	default:
		preparations.StdError(
			c,
			"Invalid subcommand - See $ jacamar-auth --help",
			ae.Msg,
			sysExit,
		)
	}
}

func establishAbstract(
	c arguments.ConcreteArgs,
	msg logging.Messenger,
	done chan struct{},
) (ae *executors.AbstractExecutor, err error) {
	ae, err = preparations.JobContext(c, sysExit)
	if err != nil {
		return nil, errorhandling.NewJobCtxError(err)
	}

	ae.Msg = msg

	err = preparations.NewAuthorizedUser(ae, true)
	if err != nil {
		return nil, errorhandling.NewAuthError(err)
	}

	if ae.Cfg.Auth().Broker.URL != "" {
		err = brokerService(ae, c)
		if err != nil {
			return nil, errorhandling.NewBrokerError(err)
		}
	}

	run, err := newRunner(ae, done)
	if err != nil {
		return nil, errorhandling.NewRunMechanismError(err)
	}
	ae.Runner = run

	return ae, nil
}

// brokerService updates an AbstractExecutor to establish and potentially override context relating
// to sensitive tokens in order to realize an optional broker service.
func brokerService(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) error {
	if c.Config != nil {
		reg := brokercomms.NewJob(
			ae.Cfg.Auth().Broker,
			ae.Env.RequiredEnv,
			ae.Auth.Username(),
		)

		if err := reg.PostJob(); err != nil {
			ae.SysLog.Error(fmt.Sprintf("Failed to establish NewJob for broker service: %v", err))
			return err
		}

		ae.Env.RequiredEnv.JobToken = reg.JobBrokerToken()
		ae.Env.StatefulEnv.BrokerToken = reg.JobBrokerToken()
		ae.SysLog.Debug("Successfully registered job with broker service")
	} else {
		ae.Env.RequiredEnv.JobToken = ae.Env.StatefulEnv.BrokerToken
	}

	return nil
}

func newRunner(
	ae *executors.AbstractExecutor,
	done chan struct{},
) (run runmechanisms.Runner, err error) {
	absCmdr := command.NewAbsCmdr(ae.Cfg.General(), ae.Msg, done)
	absCmdr.EnableNotifyTerm()

	opt := ae.Cfg.Auth()
	if opt.Downscope == "" {
		return nil, fmt.Errorf(
			"invalid runner configuration, downscope must be defined",
		)
	}

	var cmdr command.Commander
	switch opt.Downscope {
	case "setuid":
		cmdr, err = (bash.Factory{AbsCmdr: absCmdr}).CreateSetuidShell(
			ae.Auth,
			opt.Broker.URL != "",
			ae.Env.StatefulEnv,
		)
	case "sudo":
		cmdr, err = (bash.Factory{AbsCmdr: absCmdr}).CreateSudoShell(
			ae.Auth,
			opt.Broker.URL != "",
			ae.Env.StatefulEnv,
		)
	case "none":
		cmdr, err = (bash.Factory{AbsCmdr: absCmdr}).CreateStdShell()
	default:
		err = errors.New("invalid runner configuration, ensure supported downscope is defined")
	}

	if err != nil {
		return nil, err
	}
	return basic.NewMechanism(cmdr, ae.Auth), nil
}

func prepareExec(ae *executors.AbstractExecutor) {
	if (ae.Cfg.Auth()).RootDirCreation {
		if err := rootDirManagement(ae.Auth); err != nil {
			errorhandling.MessageError(
				arguments.ConcreteArgs{Prepare: &arguments.PrepareCmd{}},
				ae.Msg,
				errorhandling.NewAuthError(fmt.Errorf("error managaing base directory: %w", err)),
			)
			sysExit()
		}
	}

	cmd := jacamarCmd(ae.Msg, ae.Cfg.Auth(), "prepare")
	if err := ae.Runner.PipeOutput(cmd); err != nil {
		ae.SysLog.Debug(
			fmt.Sprintf("Error encountered executing prepare_exec: %s", err.Error()),
		)
		sysExit()
	}
}

func runExec(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) {
	if err := ae.Runner.TransferScript(
		c.Run.Script,
		c.Run.Stage,
		ae.Env,
		ae.Cfg.Auth(),
	); err != nil {
		ae.SysLog.Warning(
			fmt.Sprintf("Failed to transfer runner job script: %s", err.Error()),
		)

		preparations.StdError(c, strings.Join([]string{
			"failure to transfer script",
			c.Run.Script,
			"during stage",
			c.Run.Stage,
		}, " "), ae.Msg, sysExit)
	}

	cmd := jacamarCmd(ae.Msg, ae.Cfg.Auth(), fmt.Sprintf("run env-script %s", c.Run.Stage))
	if err := ae.Runner.PipeOutput(cmd); err != nil {
		ae.SysLog.Debug(strings.Join([]string{
			"Error encountered executing Jacamar command (",
			cmd,
			") during run_exec: ",
			err.Error(),
		}, ""))

		buildExit()
	}
}

func cleanupExec(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) {
	ae.Runner.CommandDir(ae.Auth.HomeDir()) // Avoid potential issues with missing data_dir.

	stdin := jacamarCmd(
		ae.Msg,
		ae.Cfg.Auth(),
		"cleanup",
	) + " --configuration " + c.Cleanup.Configuration
	if err := ae.Runner.PipeOutput(stdin); err != nil {
		ae.SysLog.Debug(
			fmt.Sprintf("Error encountered executing cleanup_exec: %s", err.Error()),
		)

		sysExit()
	}
}

// jacamarCmd builds a valid stdin to invoke Jacamar via a defined run mechanism.
func jacamarCmd(msg logging.Messenger, auth configure.Auth, stage string) string {
	var b bytes.Buffer

	if auth.SourceScript != "" {
		b.WriteString(fmt.Sprintf("source %s && ", filepath.Clean(
			strings.TrimSpace(auth.SourceScript))))
	}
	b.WriteString("exec ")

	if auth.JacamarPath != "" {
		b.WriteString(filepath.Clean(strings.TrimSpace(auth.JacamarPath)))
		if !strings.HasSuffix(b.String(), "jacamar") {
			if !strings.HasSuffix(auth.JacamarPath, "/") {
				b.WriteString("/")
			}
			b.WriteString(jacamarApp)
		}
	} else {
		path, err := exec.LookPath(jacamarApp)
		if err != nil {
			msg.Warn(fmt.Sprintf(
				"unable to identify %s on path during authorization, reverting to default",
				jacamarApp,
			))

			// When nothing is found default to /usr/bin as JacamarPath
			b.WriteString("/usr/bin/")
			b.WriteString(jacamarApp)
		}
		b.WriteString(path)
	}

	b.WriteString(" " + stage)
	return b.String()
}

// rootDirManagement creates/manages the root directory for an authorization user during
// the privileged authorization process.
func rootDirManagement(auth authuser.Authorized) error {
	dir := auth.BaseDir()
	info, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if os.IsNotExist(err) {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Mkdir(dir, 0700); err != nil {
			return err
		}
	} else if info.Mode().String() != "drwx------" {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Chmod(dir, 0700); err != nil {
			return err
		}
	}

	// enforce permissions
	uid, gid, err := auth.IntegerID()
	if err != nil {
		return err
	}

	return os.Chown(dir, uid, gid)
}

func init() {
	// Initialize exit status in order to facilitate potential failures at any stage.
	sys, build := envparser.ExitCodes()
	sysExit = func() { os.Exit(sys) }
	buildExit = func() { os.Exit(build) }
}
