package authuser

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

// IdentifyDirectories updates the appropriate directories in the UserContext (Basedir,
// BuildsDir, CacheDir), returning an error if encountered in the process. A fully
// validated user's context is required before invoking this method. No directories
// are created as part of this process.
func (u *UserContext) identifyDirectories(
	gen configure.General,
	req envparser.RequiredEnv,
	jwtClaims gitlabjwt.Claims,
) error {
	if u.homeDir == "" || u.username == "" {
		return fmt.Errorf(
			"incomplete user context detected, aborting build directory identification",
		)
	}

	if gen.DataDir == "$HOME" {
		u.prepHomeDataDir(gen, req, jwtClaims)
		return nil
	} else if gen.DataDir != "" {
		u.prepDefinedDataDir(gen, req, jwtClaims)
		return nil
	}

	// Missing upstream context for the defined (config.toml) build/cache directory.
	return fmt.Errorf(
		"undefined 'data_dir' unsupported, please updated your configuration",
	)
}

func (u *UserContext) prepHomeDataDir(
	gen configure.General,
	req envparser.RequiredEnv,
	jwtClaims gitlabjwt.Claims,
) {
	u.baseDir = u.homeDir + "/.jacamar-ci"
	u.buildsDir = buildDir(u.baseDir, req)
	u.cacheDir = cacheDir(u.baseDir)
	u.scriptDir = scriptDir(u.baseDir, req, jwtClaims)

	if gen.CustomBuildDir {
		base := customBuildsDir(u.homeDir)
		if base != "" {
			base = strings.Join([]string{base, "/", u.username}, "")
			u.buildsDir = buildDir(base, req)
		}
	}
}

func (u *UserContext) prepDefinedDataDir(
	gen configure.General,
	req envparser.RequiredEnv,
	jwtClaims gitlabjwt.Claims,
) {
	u.baseDir = strings.TrimSpace(gen.DataDir) + "/" + u.username
	u.buildsDir = buildDir(u.baseDir, req)
	u.cacheDir = cacheDir(u.baseDir)
	u.scriptDir = scriptDir(u.baseDir, req, jwtClaims)

	if gen.CustomBuildDir {
		base := customBuildsDir(u.homeDir)
		if base != "" {
			base = strings.Join([]string{base, "/", u.username}, "")
			u.buildsDir = buildDir(base, req)
		}
	}
}

// buildDir returns target for CI job's DefaultBuildDir:
// <base_dir>/builds/<runner-short>/<concurrent-id>.
func buildDir(base string, req envparser.RequiredEnv) string {
	cID := req.ConcurrentID
	for len(cID) < 3 {
		cID = "0" + cID
	}
	return strings.Join([]string{base,
		"/builds/", req.RunnerShort,
		"/", cID}, "")
}

// cacheDir returns target for CI job's DefaultCacheDir:
// <base_dir>/cache.
func cacheDir(base string) string {
	return strings.Join([]string{base, "/cache"}, "")
}

// scriptDir returns target for CI job's DefaultCacheDir:
// <base_dir>/scripts/<project-path>/<CI_JOB_ID>.
func scriptDir(base string, req envparser.RequiredEnv, jwtClaims gitlabjwt.Claims) string {
	return strings.Join([]string{base,
		"/scripts/", req.RunnerShort,
		"/", req.ConcurrentID,
		"/", jwtClaims.ProjectPath,
		"/", req.JobID}, "")
}

func customBuildsDir(home string) string {
	dir, ok := os.LookupEnv(envparser.UserEnvPrefix + "CUSTOM_CI_BUILDS_DIR")
	if !ok {
		return ""
	}

	dir = strings.Replace(dir, "$HOME", home, 1)
	return filepath.Clean(strings.TrimSpace(dir))
}
