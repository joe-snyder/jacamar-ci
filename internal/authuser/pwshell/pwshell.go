// +build darwin

package pwshell

import (
	"fmt"
)

// FetchShell targeting Darwin is not support and will generate an error.
func FetchShell(uid string) (string, error) {
	return "", fmt.Errorf("shell allowlist only supported on Linux")
}
