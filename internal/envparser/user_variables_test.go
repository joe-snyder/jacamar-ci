package envparser

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

// All these tests are incredibly basic when we consider the lack of complexity
// with regards to the actions these functions are preforming. However, I think
// it is important to enforce functionality when it comes to the user interactions.
// Changes to any of these should be caught via clearly defined tests before
// they reach runtime. Regardless if the changes was accidental or desired, enforcing
// the expected behavior for user interactions is important.

func TestNoBashProfile(t *testing.T) {
	tests := map[string]envTests{
		"Undefined JACAMAR_NO_BASH_PROFILE env": {
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
		"Defined true JACAMAR_NO_BASH_PROFILE env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE": "true",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
		"Defined 1 JACAMAR_NO_BASH_PROFILE env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE": "1",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
		"Defined false JACAMAR_NO_BASH_PROFILE env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE": "false",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got := NoBashProfile()

			if tt.assertBoolean != nil {
				tt.assertBoolean(t, got)
			}
		})
	}
}

func TestEtcProfileOnly(t *testing.T) {
	tests := map[string]envTests{
		"Undefined JACAMAR_ETC_PROFILE_ONLY env": {
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
		"Defined true JACAMAR_ETC_PROFILE_ONLY env": {
			targetEnv: map[string]string{
				UserEnvPrefix + "JACAMAR_ETC_PROFILE_ONLY": "true",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got := EtcProfileOnly()

			if tt.assertBoolean != nil {
				tt.assertBoolean(t, got)
			}
		})
	}
}

func TestTrustJobToken(t *testing.T) {
	t.Run("verify CI_JOB_TOKEN retrieved", func(t *testing.T) {
		os.Setenv("TRUSTED_CI_JOB_TOKEN", "T0k3n")
		defer os.Unsetenv("TRUSTED_CI_JOB_TOKEN")

		got := TrustJobToken()
		assert.Equal(t, "T0k3n", got)
	})
}
