package usertools

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
)

type dirTests struct {
	dir string
	au  *mock_authuser.MockAuthorized

	mockCurUsrFactory func() (*user.User, error)

	assertError    func(*testing.T, error)
	assertCreation func(*testing.T, []string)
}

func mocksFunctional(ctrl *gomock.Controller, dir string) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BaseDir().Return(dir).AnyTimes()
	m.EXPECT().BuildsDir().Return(dir + "/builds").AnyTimes()
	m.EXPECT().CacheDir().Return(dir + "/cache").AnyTimes()
	m.EXPECT().ScriptDir().Return(dir + "/script").AnyTimes()
	return m
}

func mocksEmptyBase(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BaseDir().Return("").MaxTimes(1)
	m.EXPECT().BuildsDir().Return("/var/tmp/ci/builds").MaxTimes(1)
	m.EXPECT().CacheDir().Return("/var/tmp/ci/cache").MaxTimes(1)
	m.EXPECT().ScriptDir().Return("/var/tmp/ci/script").MaxTimes(1)
	return m
}

func mocksInvalidBuilds(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BaseDir().Return("/var/tmp/ci").AnyTimes()
	m.EXPECT().BuildsDir().Return("/var/tmp").AnyTimes()
	m.EXPECT().CacheDir().Return("/var/tmp/ci/cache").AnyTimes()
	m.EXPECT().ScriptDir().Return("/var/tmp/ci/script").AnyTimes()
	return m
}

func TestCreateDirectories(t *testing.T) {
	baseDir, _ := ioutil.TempDir(t.TempDir(), "ci-dir")
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]dirTests{
		"undefined basedir": {
			au: mocksEmptyBase(ctrl),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "no base directory defined, verify runner configuration")
			},
		},
		"functional directory creation process": {
			au: mocksFunctional(ctrl, baseDir),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCreation: func(t *testing.T, dirs []string) {
				for _, d := range dirs {
					info, err := os.Stat(d)
					assert.NoError(t, err, "")
					assert.Equal(t, "drwx------", info.Mode().String())
				}
			},
		},
		"invalid custom builds directory defined": {
			au: mocksInvalidBuilds(ctrl),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := CreateDirectories(tt.au)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertCreation != nil {
				dirs := []string{
					tt.au.BaseDir(),
					tt.au.BuildsDir(),
					tt.au.CacheDir(),
					tt.au.ScriptDir(),
				}
				tt.assertCreation(t, dirs)
			}
		})
	}
}

func Test_verifyBasePermissions(t *testing.T) {
	goodDir, _ := ioutil.TempDir(t.TempDir(), "ci-permissions")
	badPerm, _ := ioutil.TempDir(t.TempDir(), "bad-permissions")
	_ = os.Chmod(badPerm, 0706) // Bypass syscall to umask

	usr, _ := user.Current()

	tests := map[string]dirTests{
		"correct directory permissions found": {
			dir: goodDir,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"home directory specified": {
			dir: usr.HomeDir,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid permissions (!= 700) found on directory": {
			dir: badPerm,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"empty target directory specified": {
			dir: "",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "no base directory defined, verify runner configuration")
			},
		},
		"invalid current user detected": {
			dir: goodDir,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			mockCurUsrFactory: func() (*user.User, error) {
				return nil, errors.New("no current user")
			},
		},
		"invalid ownership detected": {
			dir: goodDir,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					fmt.Sprintf(
						"invalid ownership detected on directory %s, this must be manually addressed",
						goodDir,
					),
				)
			},
			mockCurUsrFactory: func() (*user.User, error) {
				return &user.User{Uid: "42"}, nil
			},
		},
		"nested directory does not exist": {
			dir: goodDir + "/builds/test",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockCurUsrFactory != nil {
				curUserFactory = tt.mockCurUsrFactory
			} else {
				curUserFactory = authuser.CurrentUser
			}

			err := verifyBasePermissions(tt.dir)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_mkdir(t *testing.T) {
	tarPath, _ := ioutil.TempDir(t.TempDir(), "mkdir")

	tests := map[string]dirTests{
		"target directory successfully created": {
			dir: tarPath,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"target directory already exists": {
			dir: tarPath,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := mkdir(tt.dir)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
