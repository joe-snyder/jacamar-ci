package command

import (
	"os/exec"
	"reflect"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

const (
	killDuration = 30 // backup duration
)

// Commander interface provides the lowest level mechanisms that are supported by
// Jacamar for influencing how commands/stdin is executed on the underlying or target
// system. Idly it should be interacted with via a Runner interface built on top of.
type Commander interface {
	// WithOutput executes the provided stdin. All output from the command is piped
	// directly to stdout/stderr. Bypasses the runner command and instead rely solely
	// on the its underlying base/environment layer to avoid execution on such resources.
	PipeOutput(stdin string) error
	// ReturnOutput executes the provided stdin. All output from the command is returned
	// as a string. Bypasses the runner command and instead rely solely on the its
	// underlying base/environment layer to avoid execution on such resources.
	ReturnOutput(stdin string) (string, error)
	// CommandDir changes the directory where command will be executed.
	CommandDir(dir string)
	// SigtermReceived returns a boolean based upon if a SIGTERM has been captured.
	SigtermReceived() bool
	// AppendEnv appends the provided slice to the command's environment.
	AppendEnv([]string)
}

// AbstractCommander organizes configurations for all Runner interface. Each can
// implement support for configuration in their own way with no strict enforcement
// on how they are observed.
type AbstractCommander struct {
	Mutex        sync.Mutex
	KillTimeout  time.Duration
	NotifyTerm   bool // Send SIGTERM to sub-processes
	TermCaptured bool
}

// EnableNotifyTerm safely enables the use of SIGKILL.
func (a *AbstractCommander) EnableNotifyTerm() {
	a.Mutex.Lock()
	a.NotifyTerm = true
	a.Mutex.Unlock()
}

// CloneCmd clones all values from the source exec.Cmd struct to the
// provided destination.
func (a *AbstractCommander) CloneCmd(src, dest *exec.Cmd) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()

	if src == nil || dest == nil {
		// Lets avoid a panic.
		return
	}

	val := reflect.ValueOf(src)
	if val.Kind() == reflect.Ptr {
		ex := val.Elem()
		y := reflect.New(ex.Type())
		ey := y.Elem()
		ey.Set(ex)
		reflect.ValueOf(dest).Elem().Set(y.Elem())
	}
}

// RunCmd runs the provided exec.Cmd and integrates monitoring for SIGTERM into waiting for
// any spawned processes to complete.
func (a *AbstractCommander) RunCmd(cmd *exec.Cmd) (err error) {
	stopMonitor := make(chan struct{})
	cmdErr := make(chan error)
	defer close(stopMonitor)
	defer close(cmdErr)

	go a.MonitorTermination(cmd, stopMonitor)

	err = cmd.Start()
	if err != nil {
		return err
	}

	go func() {
		err = cmd.Wait()
		cmdErr <- err
	}()

	select {
	case <-stopMonitor:
		// If required the process will be terminated by MonitorTermination function.
	case err = <-cmdErr:
	}

	return
}

// NewAbsCmdr generate a valid AbstractCommander structure with application defaults
// establish signal (SIGTERM) monitoring.
func NewAbsCmdr(
	gen configure.General,
	msg logging.Messenger,
	done chan struct{},
) *AbstractCommander {
	t, err := time.ParseDuration(gen.KillTimeout)
	if err != nil {
		// backup if invalid time duration provided
		t = killDuration * time.Second
		msg.Warn("Invalid time duration in configuration (KillTimeout), defaults to 30s.")
	}
	a := &AbstractCommander{
		NotifyTerm:   false,
		KillTimeout:  t,
		TermCaptured: false,
	}

	go a.MonitorSignal(done)

	return a
}
