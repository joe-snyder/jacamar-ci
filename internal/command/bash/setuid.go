package bash

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"syscall"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

func (s *shell) targetValidUser(
	au authuser.Authorized,
	broker bool,
	state envparser.StatefulEnv,
) error {
	uid, gid, err := au.IntegerID()
	if err != nil {
		return err
	}

	if s.cmd == nil || s.cmd.SysProcAttr == nil {
		return errors.New(
			"required cmd structure is nil, ensure proper generation in build()",
		)
	}

	s.cmd.Env = safeEnv(au, broker, state.BrokerToken)

	s.cmd.SysProcAttr.Credential = &syscall.Credential{
		Uid:         uint32(uid),
		Gid:         uint32(gid),
		Groups:      []uint32{},
		NoSetGroups: true,
	}

	return nil
}

func safeEnv(au authuser.Authorized, broker bool, brokerToken string) []string {
	var safe []string
	for _, e := range os.Environ() {
		if envparser.SupportedPrefix(e) {
			if strings.HasPrefix(e, envparser.UserEnvPrefix+"CI_JOB_JWT") {
				// Jacamar does not required CI_JOB_JWT so remove by default,
				continue
			}

			if broker {
				e = strings.ReplaceAll(e, envparser.TrustJobToken(), brokerToken)
			}

			safe = append(safe, e)
		}
	}

	name := au.Username()
	sys, build := envparser.ExitCodes()

	safe = append(safe, []string{
		fmt.Sprintf("%s=%d", envparser.SysCodePrefix, sys),
		fmt.Sprintf("%s=%d", envparser.BuildCodePrefix, build),
		fmt.Sprintf("USER=%s", name),
		fmt.Sprintf("LOGNAME=%s", name),
		fmt.Sprintf("HOME=%s", name),
		"PATH=/usr/bin:/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin",
	}...)

	return safe
}
