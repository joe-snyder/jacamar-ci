package bash

import (
	"errors"
	"os/exec"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_command"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type bashTests struct {
	broker    bool
	encoded   string
	script    string
	state     envparser.StatefulEnv
	targetEnv map[string]string

	s       *shell
	absCmdr *command.AbstractCommander
	auth    *mock_authuser.MockAuthorized
	cmdr    *mock_command.MockCommander

	assertCommand func(*testing.T, *exec.Cmd)
	assertEnv     func(*testing.T, []string)
	assertError   func(*testing.T, error)
	assertShell   func(*testing.T, *shell)
	assertString  func(*testing.T, string)
}

func mockAuthWorkingID(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().Username().Return("user").AnyTimes()
	m.EXPECT().HomeDir().Return("/home/user").AnyTimes()
	m.EXPECT().IntegerID().Return(1001, 2002, nil).AnyTimes()
	return m
}

func mockAuthErrorID(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().IntegerID().Return(0, 0, errors.New("error message")).MaxTimes(1)
	return m
}

func Test_shell_PipeOutput(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	bash, _ := (Factory{command.NewAbsCmdr(cfg.General(), m, done)}).CreateStdShell()

	tests := map[string]bashTests{
		"nil command value provided": {
			s: &shell{cmd: nil},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "command (exec.Cmd) not found, verify properly built")
			},
		},
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.s.PipeOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_shell_ReturnOutput(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	bash, _ := (Factory{command.NewAbsCmdr(cfg.General(), m, done)}).CreateStdShell()

	tests := map[string]bashTests{
		"nil command value provided": {
			s: &shell{cmd: nil},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "command (exec.Cmd) not found, verify properly built")
			},
		},
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.s.ReturnOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func TestFactory_CreateStdShell(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	tests := map[string]bashTests{
		"avoid panic by catching improper factory": {
			absCmdr: nil,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"AbstractCommander structure is nil, verify factory for CreateStdShell",
				)
			},
		},
		"valid standard bash command generated": {
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				bp, _ := exec.LookPath("bash")
				assert.Equal(t, bp, s.cmd.Path)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{tt.absCmdr}
			s, err := f.CreateStdShell()

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestFactory_CreateBaseShell(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	auth := mock_authuser.NewMockAuthorized(ctrl)
	auth.EXPECT().HomeDir().Return("/home/user").Times(3)

	tests := map[string]bashTests{
		"avoid panic by catching improper factory": {
			absCmdr: nil,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"AbstractCommander structure is nil, verify factory for CreateBaseShell",
				)
			},
		},
		"valid base bash command generated": {
			auth:    auth,
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				ep, _ := exec.LookPath("env")
				assert.Equal(t, ep, s.cmd.Path)
			},
		},
		"base command generated, no Bash profiled enabled": {
			targetEnv: map[string]string{
				envparser.UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE": "true",
			},
			auth:    auth,
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				// Specifying --noprofile will result in no PATH.
				out, err := s.ReturnOutput("env | grep PATH | echo pass")
				assert.NoError(t, err)
				assert.Equal(t, "pass\n", out)
			},
		},
		"base command generated, source /etc/profile only": {
			targetEnv: map[string]string{
				envparser.UserEnvPrefix + "JACAMAR_ETC_PROFILE_ONLY": "true",
			},
			auth:    auth,
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				// Source /etc/project will establish a PATH.
				out, err := s.ReturnOutput("env | grep PATH")
				assert.NoError(t, err)
				assert.NotEmpty(t, out)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			f := Factory{tt.absCmdr}
			s, err := f.CreateBaseShell(tt.auth)

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestFactory_CreateSetuidShell(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	tests := map[string]bashTests{
		"avoid panic by catching improper factory": {
			absCmdr: nil,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"AbstractCommander structure is nil, verify factory for CreateSetuidShell",
				)
			},
		},
		"error attempting to validate target use context": {
			auth:    mockAuthErrorID(ctrl),
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to generate setuid command: error message",
				)
			},
		},
		"valid setuid bash command generated": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				bp, _ := exec.LookPath("bash")
				assert.Equal(t, bp, s.cmd.Path)
				assert.Equal(t, uint32(1001), s.cmd.SysProcAttr.Credential.Uid)
			},
		},
		"SigtermReceived functions correctly against defaults": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.False(t, s.SigtermReceived(), "default SigtermReceived must be false")
			},
		},
		"underlying command AppendEnv properly function": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s)
				s.AppendEnv([]string{"FOO=BAR"})
				assert.Contains(t, s.cmd.Env, "FOO=BAR")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{tt.absCmdr}
			s, err := f.CreateSetuidShell(tt.auth, tt.broker, tt.state)

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestFactory_CreateSudoShell(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	tests := map[string]bashTests{
		"avoid panic by catching improper factory": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"AbstractCommander structure is nil, verify factory for CreateSudoShell",
				)
			},
		},
		"sudo su shell created": {
			auth:    mockAuthWorkingID(ctrl),
			absCmdr: command.NewAbsCmdr(cfg.General(), m, done),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Equal(t, s.cmd.Args, []string{
					"sudo", "-E", "su", "user",
					"-m", "-s", "/bin/bash", "--pty", "-c",
				})
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{tt.absCmdr}
			s, err := f.CreateSudoShell(tt.auth, tt.broker, tt.state)

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
