#!/usr/bin/env bash

# Deploy Spack at the specified $SPACK_ROOT locations and $SPACK_COMMIT (sha)

set -eo pipefail
set +o noclobber

if [ -z $SPACK_ROOT ]; then
    echo "SPACK_ROOT environment variable must be set"
    exit 1
fi

if [ -z $SPACK_COMMIT ]; then
    echo "SPACK_COMMIT environment variable must be set"
    exit 1
fi

mkdir -p $SPACK_ROOT/{opt,cache,mcache,spack}
chmod 700 $SPACK_ROOT

pushd $SPACK_ROOT/spack
    git init
    git remote set-url origin https://github.com/spack/spack.git || git remote add origin https://github.com/spack/spack.git
    git fetch origin --quiet --prune +refs/heads/*:refs/remotes/origin/* +refs/tags/*:refs/tags/*
    git checkout --force --quiet ${SPACK_COMMIT}
    git rev-parse HEAD
    source share/spack/setup-env.sh
popd
