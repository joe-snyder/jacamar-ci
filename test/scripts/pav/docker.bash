#!/usr/bin/env bash

# Execute Pavilion tests within Docker container.

set -eo pipefail
set +o noclobber

[ -z "${ROOT_DIR}" ] && ROOT_DIR=$(pwd)
export PAV_CONFIG_DIR="${ROOT_DIR}/test/pavilion/docker"

ci_dir="/builds/ecp-ci/jacamar-ci"

test() {
  chmod -R 755 "${ROOT_DIR}/test/scripts"
  chmod 777 "${ROOT_DIR}/test/pavilion/docker/working_dir"

  pav_image="registry.gitlab.com/ecp-ci/jacamar-ci/pav-tester:dde1d79-13.9"
  docker pull ${pav_image}

  build_image="registry.gitlab.com/ecp-ci/jacamar-ci/centos7-builder:latest"
  docker pull ${build_image}

  echo "Building Jacamar Binaries..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -w ${ci_dir} \
    -t ${build_image} \
    bash -c "make build"

  echo "Testing Jacamar + Shell Executor..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -u "user" \
    -t ${pav_image} \
    bash -c "nohup python -u ${ci_dir}/tools/mock-gl-api/gl-api.py \
              > ${ci_dir}/test/pavilion/docker/working_dir/mock_api & \
            python ${ci_dir}/test/pavilion/shared/sequence.py \
              ${ci_dir}/test/pavilion/docker/tests/jacamar.yaml \
              ${ci_dir}/test/pavilion/docker"

  echo "Testing Jacamar-Auth..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t  ${pav_image} \
    bash -c "nohup python -u ${ci_dir}/tools/mock-gl-api/gl-api.py \
              > ${ci_dir}/test/pavilion/docker/working_dir/mock_api & \
            python ${ci_dir}/test/pavilion/shared/sequence.py \
              ${ci_dir}/test/pavilion/docker/tests/auth.yaml \
              ${ci_dir}/test/pavilion/docker"

  echo "Review Results..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t ${pav_image} \
    bash -c "cd ${ci_dir}/test/pavilion/docker \
            && pav status --all --limit 1000 \
            && pav status -a -l 1000 | if grep -q FAIL; then exit 1; fi"
}

clean() {
  pushd "${PAV_CONFIG_DIR}/working_dir"
    ls | grep -v .gitignore | xargs rm -rf
  popd
}

 case "${1}" in
  clean)
    clean
    ;;
  test)
    test
    ;;
  *)
    echo "Invalid argument: ${0} (test|clean)"
    exit 1
    ;;
 esac
