#!/usr/bin/env python

import os
import requests

project = os.getenv('BUILDSTATUS_PROJECT')
api = os.getenv('BUILDSTATUS_APIURL')
token = os.getenv('BUILDSTATUS_TOKEN')
name = os.getenv('BUILDSTATUS_JOB')
sha = os.getenv('CI_COMMIT_SHA')
state = os.getenv('CI_JOB_NAME')
url = os.getenv('CI_PIPELINE_URL')

# https://docs.gitlab.com/ee/api/commits.html#post-the-build-status-to-a-commit
status = {
    'id': project,
    'sha': sha,
    'state': state,
    'name': name,
    'target_url': url
}

try:
    r = requests.post("{}/projects/{}/statuses/{}".format(api, project, sha),
                    headers={'PRIVATE-TOKEN': token},
                    data=status)
    print(r.text)
except requests.exceptions.Timeout:
    raise SystemError("Requests error, timeout")
except requests.exceptions.TooManyRedirects:
    raise SystemError("Requests error, too many redirects")
except requests.exceptions.HTTPError as err:
    raise SystemError(err)
