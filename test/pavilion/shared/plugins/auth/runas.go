package main

import (
	"errors"

	jp "gitlab.com/ecp-ci/jacamar-plugins"
)

func Validate(ri jp.RunAsInit) (over jp.RunAsOverride, err error) {
	if ri.CurrentUser == "user" {
		over.Username = "passtest"
	} else {
		err = errors.New("test error")
	}

	return
}
