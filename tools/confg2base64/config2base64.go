package main

import (
	"bytes"
	"encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"

	"github.com/BurntSushi/toml"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
)

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		log.Fatal("requires single toml file specified as argument")
	}

	contents, err := ioutil.ReadFile(filepath.Clean(flag.Arg(0)))
	if err != nil {
		log.Fatal("failed to load configuration: ", err)
	}

	options, err := loadContents(string(contents))
	if err != nil {
		log.Fatal("failed to load contents: ", err)
	}

	encoded, err := encode(options)
	if err != nil {
		log.Fatal("failed to encoded options, ", err)
	}

	fmt.Println(encoded)
}

func loadContents(contents string) (configure.Options, error) {
	var cfg configure.Options
	r := strings.NewReader(contents)
	if _, err := toml.DecodeReader(r, &cfg); err != nil {
		return configure.Options{}, err
	}
	return cfg, nil
}

func encode(opt configure.Options) (string, error) {
	buf := new(bytes.Buffer)
	if err := toml.NewEncoder(buf).Encode(opt); err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(buf.Bytes()), nil
}
