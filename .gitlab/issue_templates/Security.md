## Summary

<!-- Brief summary of issue. -->

### Scope of Issue

<!-- Scope of affected facilities/users and priority presented. -->

## Steps to reproduce

<!-- Steps that can be taken to reproduce the issue. -->

## Expected behavior

<!-- Description of what is expected. -->

## Possible fix

<!-- Possible source of the bug or potential steps to fix. -->

/label ~Security

/assign @paulbry

<!-- Security related issues default to confidential, can be manually changed later. -->
/confidential
