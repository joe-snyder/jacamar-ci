%{!?_version: %define _version 13.8.0 }

Name:           gitlab-runner-ecp
Version:        %{_version}
Release:        1%{?dist}
Summary:        GitLab Runner %{version} with ECP supported patches.
Vendor:         Exascale Computing Project - 2.4.4
Packager:       ECP CI Infrastructure <ecp-ci-infrastructure@elist.ornl.gov>

# License details: https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/LICENSE
License:        MIT
URL:            https://gitlab.com/gitlab-org/gitlab-runner
Source0:        https://gitlab.com/gitlab-org/gitlab-runner/-/archive/v%{version}/gitlab-runner-v%{version}.tar.gz
Patch0:         runner_trusted.patch

# Go is required for build but a newer version (see go.mod)
# than is available on most distributions.
BuildRequires:  bash
BuildRequires:  curl
BuildRequires:  make
BuildRequires:  tar

Requires:       curl
Requires:       git
Requires:       tar

Conflicts:      gitlab-runner
Conflicts:      gitlab-runner-beta
Conflicts:      gitlab-ci-multi-runner
Conflicts:      gitlab-ci-multi-runner-beta

Provides:       gitlab-ci-multi-runner = %{version}-1
Provides:       gitlab-runner = %{version}-1

%global glib %{_libdir}/gitlab-runner
%global getc /etc/gitlab-runner

%description
The GitLab Runner is used in conjunctions with a GitLab server to
execute CI/CD jobs and convey ongoing results back. This release
contains ECP focused patches that enable support for the custom
executor driver Jacamar CI.

%prep
%if %{defined _sha256sum}
echo "%{_sha256sum}  %SOURCE0" | sha256sum -c -
%endif
%setup -q -n gitlab-runner-v%{version}

%patch0 -p1

%build

make runner-bin-host VERSION=%{version}.ecp

%install

mkdir -p %{buildroot}%{getc}
mkdir -p %{buildroot}%{glib}
mkdir -p %{buildroot}%{_bindir}

install -m 755 out/binaries/gitlab-runner %{buildroot}%{glib}/gitlab-runner

ln -sf %{glib}/gitlab-runner %{buildroot}%{_bindir}/gitlab-ci-multi-runner
ln -sf %{glib}/gitlab-runner %{buildroot}%{_bindir}/gitlab-runner

%files

%attr(0700, root, root) %{getc}
%attr(0755, root, root) %{glib}
%{_bindir}/gitlab-runner
%{_bindir}/gitlab-ci-multi-runner
