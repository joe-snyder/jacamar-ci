# Jacamar CI Spack Package

To better support the deployment and testing of Jacamar/GitLab
on HPC systems we would like to add support for
[Spack](https://github.com/spack/spack) packages. At this time
however we are not prepared to request adding the necessary packages
to the official repo. As such you can add this custom `jacamar-repo`
manually to your instances:

```console
$ spack repo add $(pwd)/jacamar-repo
==> Added repo with namespace 'jacamar-repo'.
```

From there you can install Jacamar as well as the GitLab-Runner:

```console
spack spec jacamar-ci@develop
Input spec
--------------------------------
jacamar-ci@develop

Concretized
--------------------------------
jacamar-ci@develop%gcc@9.3.0 arch=linux-pop20-zen2
    ^gitlab-runner@13.2.1%gcc@9.3.0+jacamar patches=51893933d77ae0bf037dbab852d2038a8095b2ad593c3fc8fda5d203b09e8742 arch=linux-pop20-zen2
        ^git@2.27.0%gcc@9.3.0~tcltk arch=linux-pop20-zen2
        ^go@1.14.5%gcc@9.3.0 arch=linux-pop20-zen2
            ^go-bootstrap@1.4-bootstrap-20171003%gcc@9.3.0 arch=linux-pop20-zen2
        ^tar@1.32%gcc@9.3.0 arch=linux-pop20-zen2
            ^libiconv@1.16%gcc@9.3.0 arch=linux-pop20-zen2
```

The `+jacamar` variant for the GitLab runner will apply the required patch.

To remove the repository simply `spack repo remove jacamar-repo`.
