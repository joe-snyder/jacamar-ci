# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os
import re
from spack import *


class JacamarCi(MakefilePackage):
    """HPC focused CI/CD driver for the GitLab custom executor."""

    homepage = "https://gitlab.com/ecp-ci/jacamar-ci"
    url      = "https://gitlab.com/ecp-ci/jacamar-ci/-/archive/v0.3.2/jacamar-ci-v0.3.2.tar.gz"
    git      = "https://gitlab.com/ecp-ci/jacamar-ci.git"

    maintainers = ['paulbry']
    executables = ['jacamar', 'jacamar-auth']

    version('develop', branch='develop')
    version('0.4.1', sha256='f2f6a34ce49d141bb6a7855d5e5ba7886f7e4bff393a17204c1479b650d6fa31')
    version('0.4.0', sha256='df73480db56c99e0ca998dda91d8513289a3daef561fb9c64f270cac667b3023')
    version('0.3.2', sha256='882585b3a8a43d2dcc02e39e1e389a64c4857496250a4c5e66800dbbae85684e')

    depends_on('go@1.15:', type=('build'))
    depends_on('gitlab-runner+jacamar', type=('run'))
    
    conflicts('platform=darwin', msg='Jacamar CI does not support MacOS')
    
    @classmethod
    def determine_spec_details(cls, prefix, exes_in_prefix):
        exe_to_path = dict(
            (os.path.basename(p), p) for p in exes_in_prefix
        )
        if 'jacamar' not in exe_to_path:
            return None

        jacamar = spack.util.executable.Executable(exe_to_path['jacamar'])
        output = jacamar('--version', output=str)
        if output:
            match = re.search(r'Version:\s*(\S+)', output)
            if match:
                version_str = match.group(1)
                return Spec('jacamar@{0}'.format(version_str))

    def build(self, spec, prefix):
        env['GOPATH'] = '{0}/go-path'.format(self.stage.source_path)
        mkdir(env['GOPATH'])
        if spec.version == Version('develop'):
            make('build') 
        else:
            make('build', 'VERSION={0}'.format(spec.version))
    
    def install(self, spec, prefix):
        make('install', 'PREFIX={0}'.format(prefix))

    def _test_target(self, target_exe):
        reason = "test installation of {0}".format(target_exe)
        self.run_test(target_exe, ['--version'], [], status=0,
                      installed=True, purpose=reason, skip_missing=False, work_dir='.')

    def test(self):
        """ Perform smoke test on the installation."""
        self._test_target('jacamar-auth')
        self._test_target('jacamar')
